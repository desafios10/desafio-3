function newArray(array) {
    array.sort((a, b) => a - b);

    let arrayPos = [];
    let arrayNeg = [];

    for (let i = 0; i < array.length; i++) {
        if (array[i] < 0) {
            arrayNeg.push(array[i]);
        } else if (array[i] > 0) {
            arrayPos.push(array[i]);
        }
    }
    
    let result = []

    if (arrayNeg.length%2 === 0) {
        for (negativeNum in arrayNeg) {
            result.push(arrayNeg[negativeNum]);
        }
    } else {
        for (let j = 0; j < (arrayNeg.length - 1); j++) {
            result.push(arrayNeg[j]);
        }
    }

    for (positiveNum in arrayPos) {
        result.push(arrayPos[positiveNum])
    } 

    return result;
}

const x = [-6, 4, -5, 8, -10, 0, 8];

console.log(newArray(x));